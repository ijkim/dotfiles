;;; init.el --- Simple Emacs configuration intended for beginners
;;;
;;; Andrew DeOrio <awdeorio@umich.edu> 2021
;;; Find my entire init.el here:
;;; https://github.com/awdeorio/dotfiles/blob/master/.emacs.d/init.el

;;; Commentary

;; Package Management.  Configure the built-in emacs package manager to use
;; several publicly available repositories.

;;; Code:
(setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(package-initialize)

;; Bootstrap 'use-package' and enable it.  Later, 'use-package- will
;; download and install third-party packages automatically.
;; http://cachestocaches.com/2015/8/getting-started-use-package/
;;
;; EXAMPLE:
;; (use-package foo-mode
;;   :after bar      ; load after bar package
;;   :mode "\\.foo"  ; load and enable foo-mode for *.foo files
;;   :init           ; run this code when init.el is read
;;   :config         ; run this code after loading foo-mode
;;   :ensure t       ; automatically install foo-mode if not present
;;   :defer t        ; defer loading for performance (usually the default)
;; )
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(setq use-package-always-ensure t)
;; LLDB support
(use-package realgud
  :ensure t
  )
(use-package realgud-lldb
  :ensure t
  )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("f703efe04a108fcd4ad104e045b391c706035bce0314a30d72fbf0840b355c2c" default))
 '(package-selected-packages
   '(auto-complete-c-headers plantuml-mode lsp-p4 lsp-latex auto-complete-auctex company-auctex auctex consult-lsp exec-path-from-shell rubocop solargraph @ company-lsp ccls eglot which-key dap-mode lsp-treemacs lsp-ivy helm-lsp lsp-ui lsp-mode org-bullets yasnippet-snippets magit org-journal orderless consult marginalia vertico org-roam org goto-last-point ## goto-last-change goto-char-preview undo-fu company-c-headers rufo evil-ruby-text-objects yasnippet robe rvm rinari realgud-lldb realgud company flycheck undo-tree use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-document-title ((t (:inherit default :weight bold :foreground "#c6c6c6" :font "Lucida Grande" :height 1.5 :underline nil))))
 '(org-level-1 ((t (:inherit default :weight bold :foreground "#c6c6c6" :font "Lucida Grande" :height 1.05 :line-spacing 1.5)))))

;; Remote file editing with TRAMP.  Configure TRAMP to use the same SSH
;; multiplexing that I configure in ~/.ssh/config.  By default, TRAMP ignore my
;; SSH config's multiplexing configuration, so configure the same settings here.
;; https://www.emacswiki.org/emacs/TrampMode
;; https://www.gnu.org/software/emacs/manual/html_node/tramp/Frequently-Asked-Questions.html
(use-package tramp
  :config
  (setq tramp-default-method "ssh")
  (setq tramp-ssh-controlmaster-options
        (concat
         "-o ControlPath=~/.ssh/master-%%r@%%h:%%p "
         "-o ControlMaster=auto -o ControlPersist=yes"))
  :defer 1  ; lazy loading
)

;;
;; Navigation
;;

;; Smooth scrolling (one line at a time)
(setq scroll-step 1)

;; Remove scrollbars, menu bars, and toolbars
(when (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(when (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;; https://github.com/minad/vertico
;; Enable vertico
(use-package vertico
  :ensure t
  :init
  (vertico-mode)

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (setq vertico-cycle t)
  (setq read-file-name-completion-ignore-case t
        read-buffer-completion-ignore-case t
        completion-ignore-case t)
  )

;; Optionally use the `orderless' completion style. See
;; `+orderless-dispatch' in the Consult wiki for an advanced Orderless style
;; dispatcher. Additionally enable `partial-completion' for file path
;; expansion. `partial-completion' is important for wildcard support.
;; Multiple files can be opened at once with `find-file' if you enter a
;; wildcard. You may also give the `initials' completion style a try.
(use-package orderless
  :ensure t
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :ensure t
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :ensure t
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))


;; Evil mode
(use-package evil
 :ensure t ;; install the evil package if not installed
 :init ;; tweak evil's configuration before loading it
 (setq evil-search-module 'evil-search)
 (setq evil-ex-complete-emacs-commands nil)
 (setq evil-vsplit-window-right t)
 (setq evil-split-window-below t)
 (setq evil-shift-round nil)
 (setq evil-want-C-u-scroll t)
 :config ;; tweak evil after loading it
 (evil-mode)

 ;; example how to map a command in normal mode (called 'normal state' in evil)
 (define-key evil-normal-state-map (kbd ", w") 'evil-window-vsplit)
 (define-key evil-normal-state-map (kbd ", w") 'evil-window-vsplit)
 )

;; adding "\C-r" for redo
(use-package goto-last-change
 :ensure t)
(use-package goto-last-point
 :ensure t
 :config
 (define-key evil-normal-state-map "u" 'undo-fu-only-undo)
 (define-key evil-normal-state-map "\C-r" 'undo-fu-only-redo)
 )


;; Enable richer annotations using the Marginalia package
(use-package marginalia
  :ensure t
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))


;; Example configuration for Consult
(use-package consult
  :ensure t
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi))           ;; needed by consult-line to detect isearch

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI. You may want to also
  ;; enable `consult-preview-at-point-mode` in Embark Collect buffers.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Optionally replace `completing-read-multiple' with an enhanced version.
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme
   :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-recent-file consult--source-project-recent-file consult--source-bookmark
   :preview-key (kbd "M-."))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; Optionally configure a function which returns the project root directory.
  ;; There are multiple reasonable alternatives to chose from.
  ;;;; 1. project.el (project-roots)
  (setq consult-project-root-function
        (lambda ()
          (when-let (project (project-current))
            (car (project-roots project)))))
  ;;;; 2. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-root-function #'projectile-project-root)
  ;;;; 3. vc.el (vc-root-dir)
  ;; (setq consult-project-root-function #'vc-root-dir)
  ;;;; 4. locate-dominating-file
  ;; (setq consult-project-root-function (lambda () (locate-dominating-file "." ".git")))
)

(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))



;; https://github.com/joaotavora/yasnippet
(use-package yasnippet
  :config
 ;; (yas-global-mode 1)
  (yas-reload-all)
  (add-hook 'c++-mode-hook #'yas-minor-mode)
  )
;; https://github.com/AndreaCrotti/yasnippet-snippets
(use-package yasnippet-snippets)

;;
;; Formatting
;;

;; Parentheses
(electric-pair-mode 1)                  ; automatically close parentheses, etc.
(show-paren-mode t)                     ; show matching parentheses

;; Tab settings: 2 spaces.  See also: language-specific customizations below.
(setq-default indent-tabs-mode nil)
(setq tab-width 2)

;;
;; Presentation
;;

;; Line spacing
(setq-default line-spacing 5)

;; Setting the default font size
(set-face-attribute 'default nil :font "Monaco" :height 150)
(set-face-attribute 'default t :font "Monaco")

;; Don't show a startup message
(setq inhibit-startup-message t)

;; Show line and column numbers
(setq line-number-mode t)
(setq column-number-mode t)
(setq global-hl-line-mode t)

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

;; Show syntax highlighting
(global-font-lock-mode t)

;; Highlight marked regions
(setq-default transient-mark-mode t)

(use-package powerline
  :ensure t
  :config
  (powerline-center-evil-theme)
  (display-time-mode)
  (line-number-mode)
  )

;; Mode Line
;;(use-package moody
;;  :config
;;  (setq x-underline-at-descent-line t)
;;  (moody-replace-mode-line-buffer-identification)
;;  (moody-replace-vc-mode)
;;  (moody-replace-eldoc-minibuffer-message-function))

;;;; Spacemacs dark mode
;;(use-package spacemacs-common
;;  :ensure spacemacs-theme
;;  :config (load-theme 'spacemacs-dark t)
;;  )

;; Moe Theme
(use-package moe-theme
  :ensure t
  :config
  (load-theme 'moe-dark t)
  (moe-theme-powerline)
  )

;;;; Solarized-theme
;;(use-package solarized-theme
;;  :config
;;  (load-theme 'solarized-dark t)
;;  (let ((line (face-attribute 'mode-line :underline)))
;;    (set-face-attribute 'mode-line          nil :overline   line)
;;    (set-face-attribute 'mode-line-inactive nil :overline   line)
;;    (set-face-attribute 'mode-line-inactive nil :underline  line)
;;    (set-face-attribute 'mode-line          nil :box        nil)
;;    (set-face-attribute 'mode-line-inactive nil :box        nil)
;;    (set-face-attribute 'mode-line-inactive nil :background "#f9f2d9"))
;;  )


;;
;; Mode settings
;;
;; Autocomplete for code
;; Company docs: https://company-mode.github.io/
;; Company TNG: https://github.com/company-mode/company-mode/issues/526
(use-package company
  :bind (("C-SPC" . company-search-candidates)
         :map company-active-map
         ("C-p" . company-select-previous)
         ("C-n" . company-select-next)
         :map company-search-map
         ("C-p" . company-select-previous)
         ("C-n" . company-select-next))
  :init
  (setq company-minimum-prefix-length 2
        company-idle-delay 0.0)
  (setq company-backends '((company-capf :with company-yasnippet)))
  :config
  (global-company-mode +1)
  (company-tng-mode)
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0)
  )

;;  :config
;;  (company-tng-mode t)       ; use default configuration
;;  (global-company-mode)
;;  :defer t                              ; lazy loading
;;  )

;; orgmode
(use-package org
  :ensure t
  :config
  (setq org-startup-indented t)
  (setq org-hide-leading-stars t)
  (setq org-deadline-warning-days 7)
  (setq org-log-into-drawer t)
  (setq org-log-done 'time)
  (setq org-log-reschedule 'time)
  (setq org-clock-sound "~/Documents/fairy-ding.mp3")
  (setq org-archive-location "%s_archive::")
  (setq org-capture-templates '(("p" "Private templates")
                                ("pt" "TODO entry" entry
                                 (file+headline "~/Personal/org/todo.org" "Capture")
                                 (file "~/Personal/org/tpl-todo.txt"))
                                ("pj" "Journal entry" entry
                                 (file+olp+datetree "~/Personal/org/journal.org")
                                 "* %U - %^{Activity}")
                                ))
  (setq org-agenda-files '("~/Personal/org/todo.org"))
  (setq org-ellipsis " ↓")
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 1.7))
  (global-set-key (kbd "C-c l") #'org-store-link)
  (global-set-key (kbd "C-c a") #'org-agenda)
  (global-set-key (kbd "<f6>") #'org-capture)
  (define-key org-mode-map "\M-q" 'toggle-truncate-lines)
  (add-hook 'org-mode-hook 'visual-line-mode)
  (add-hook 'org-mode-hook (lambda () (flyspell-mode 1)))
  ;;(add-hook 'org-mode-hook 'variable-pitch-mode)
  )

;; set fonts
;;(custom-theme-set-faces
;;   'user
;;   '(variable-pitch ((t (:family "ETBembo" :height 180))))
;;   '(fixed-pitch ((t ( :family "Fira Code Retina" :height 160))))
;;   '(org-block ((t (:inherit fixed-pitch))))
;;   '(org-code ((t (:inherit (shadow fixed-pitch)))))
;;   '(org-document-info ((t (:foreground "dark orange"))))
;;   '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
;;   '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
;;   '(org-link ((t (:foreground "royal blue" :underline t))))
;;   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;;   '(org-property-value ((t (:inherit fixed-pitch))) t)
;;   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
;;   '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
;;   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
;;   '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))


;; org-bullets
(use-package org-bullets
    :config
    (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(let* ((variable-tuple
          (cond ((x-list-fonts "ETBembo")         '(:font "ETBembo"))
                ((x-list-fonts "Source Sans Pro") '(:font "Source Sans Pro"))
                ((x-list-fonts "Lucida Grande")   '(:font "Lucida Grande"))
                ((x-list-fonts "Verdana")         '(:font "Verdana"))
                ((x-family-fonts "Sans Serif")    '(:family "Sans Serif"))
                (nil (warn "Cannot find a Sans Serif Font.  Install Source Sans Pro."))))
         (base-font-color     (face-foreground 'default nil 'default))
         (headline           `(:inherit default :weight bold :foreground ,base-font-color))
         )

    (custom-theme-set-faces
     'user
;     `(org-level-8 ((t (,@headline ,@variable-tuple))))
;     `(org-level-7 ((t (,@headline ,@variable-tuple))))
;     `(org-level-6 ((t (,@headline ,@variable-tuple))))
;     `(org-level-5 ((t (,@headline ,@variable-tuple))))
;     `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.0))))
;     `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.0))))
;     `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.05))))
     `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.05 :line-spacing 1.5))))
     `(org-document-title ((t (,@headline ,@variable-tuple :height 1.5 :underline nil)))))
    )
;; org-journal
(use-package org-journal
  :ensure t
  :defer t
  :init
  ;; Change default prefix key; needs to be set before loading org-journal
  (setq org-journal-prefix-key "C-c j ")
  :config
  (setq org-journal-dir "~/Personal/org/journal/"
        org-journal-date-format "%Y-%m-%d, %A"))

;; org-roam
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/Personal/roam_notes")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n r" . org-roam-ref-add)
         :map org-mode-map
         ("C-M-i"   . completion-at-point))
  :config
  (org-roam-setup)
  )

;; Magit
(use-package magit)

;; Mail mode
;;(server-start)
(add-to-list 'auto-mode-alist '("/mutt" . mail-mode))
(add-hook 'mail-mode-hook 'turn-on-auto-fill)
(add-to-list 'auto-mode-alist '(".*mutt.*" . message-mode))
(setq mail-header-separator "")
(add-hook 'message-mode-hook 'auto-fill-mode)



;;
;; MISC
;;
;; backup in one place. flat, no tree structure
(setq backup-directory-alist '(("" . "~/backup/auto-save")))

;; Enabling spell check on tex-modes
(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(dolist (hook '(c++-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

;;(add-hook 'pdf-view-mode-hook 'pdf-continuous-scroll-mode) ; did not work


;;
;; Language related
;;

(use-package lsp-mode
  :init
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (setq lsp-keymap-prefix "C-c l")
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         (c-mode . lsp)
         (c++-mode . lsp)
         (ruby-mode . lsp)
         (tex-mod . lsp)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration)
         (lsp-mode . lsp-ui-mode)
         )
  :config
  (setq lsp-enable-snippet t) 
;;  (setq lsp-completion-provider :none) 
  :commands lsp)

(use-package lsp-ui
;;  :hook ((lsp-mode . lsp-ui-mode))
  :commands lsp-ui-mode)

(use-package ccls
  :hook ((c-mode c++-mode objc-mode cuda-mode) .
         (lambda () (require 'ccls) (lsp)))
  :config
  (setq ccls-executable "/usr/local/bin/ccls")
  )

;;;; optionally
;;(use-package lsp-ui :commands lsp-ui-mode)
;;;; if you are helm user
;;;;(use-package helm-lsp :commands helm-lsp-workspace-symbol)
;;;; if you are ivy user
;;;;(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
;;(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
(use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

;; optional if you want which-key integration
(use-package which-key
    :config
    (which-key-mode))


;; C and C++ programming.
;; Intellisense syntax checking
;; http://www.flycheck.org/en/latest/
(use-package flycheck
  :config
  ;; enable in all modes
  (global-flycheck-mode)
  ;; C++11
  (add-hook 'c++-mode-hook (lambda () (setq flycheck-clang-language-standard "c++11")))
  :ensure t
  :defer t
  )

;; C and C++ programming.  Build with C-c m.  Rebuild with C-c c.  Put
;; this in c-mode-base-map because c-mode-map, c++-mode-map, and so
;; on, inherit from it.
(add-hook 'c-initialization-hook
          (lambda () (define-key c-mode-base-map (kbd "C-c m") 'compile)))
(add-hook 'c-initialization-hook
          (lambda () (define-key c-mode-base-map (kbd "C-c c") 'recompile)))
(setq-default c-basic-offset tab-width) ; indentation
(add-to-list 'auto-mode-alist '("\\.h$" . c++-mode))  ; assume C++ for .h files

;; Ruby
;;(require 'ruby-end)
(use-package ruby-end)
;; rvm
(use-package rvm
  :config
  (rvm-use-default))
;; https://github.com/dgutov/robe
(use-package robe
  :config
  (add-hook 'ruby-mode-hook 'robe-mode)
  (eval-after-load 'company
    '(push 'company-robe company-backends))
  (add-hook 'robe-mode-hook 'ac-robe-setup)
  (advice-add 'inf-ruby-console-auto :before #'rvm-activate-corresponding-ruby)
  )

(use-package rubocop
  :config
  (add-hook 'ruby-mode-hook #'rubocop-mode)
  (setq rubocop-autocorrect-on-save t)
  )
(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
  )

(use-package rvm
  :config
  (rvm-use-default))

;; LaTex
(use-package tex
  :ensure auctex
  :config
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil))

;; Global Configuration

;;
;; Key bindings
;;

;; Modified keyboard shortcuts
(global-set-key "\C-x\C-b" 'electric-buffer-list)  ; easier buffer switching

;; Dialog settings.  No more typing the whole yes or no. Just y or n
;; will do. Disable GUI dialogs and use emacs text interface.
(fset 'yes-or-no-p 'y-or-n-p)
(setq use-dialog-box nil)

;; macOS modifier keys
(setq mac-command-modifier 'meta) ; Command == Meta
(setq mac-option-modifier 'super) ; Option == Super

;; ESC once instead of ESC ESC ESC
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-c y") 'company-yasnippet)

(add-to-list 'default-frame-alist '(height . 60))
(add-to-list 'default-frame-alist '(width . 160))
(define-key key-translation-map (kbd "ESC") (kbd "C-g"))
;(define-key company-mode-map [remap indent-for-tab-command] #'company-indent-or-complete-common)


(defun yas-expand-or-company-complete (&optional arg)
  (interactive)
  (or (yas-expand) (company-indent-or-complete-common arg)))

(setq org-plantuml-jar-path (expand-file-name "/usr/local/libexec/plantuml.jar"))
(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
(org-babel-do-load-languages 'org-babel-load-languages '((plantuml . t)))

(require 'auto-complete)
(require 'auto-complete-config)
(ac-config-default)
;(defun my:ac-c-header-init ()
;  (require 'company-auto-complete-c-headers)
;  (add-to-list 'ac-sources 'ac-source-c-headers)
;  ;; gcc -xc++ -E -v -
;  (Add-to-list 'achead:include-directories '"/Library/Developer/CommandLineTools/usr/lib/clang/13.0.0/include")
;  )
;(add-hook 'c++-mode-hook 'my:ac-c-header-init)
;(add-hook 'c-mode-hook 'my:ac-c-header-init)

;;;; https://github.com/danielma/rufo.el
;;(use-package rufo
;;  :config
;;  (add-hook 'ruby-mode-hook 'rufo-minor-mode)
;;  (setq rufo-minor-mode-use-bundler t)
;;  )
;; https://github.com/porras/evil-ruby-text-objects
;;(add-hook 'ruby-mode-hook 'evil-ruby-text-objects-mode)

;;
;; Inline browser, it works well but requires a server daemon
;; cd /Users/ij/.emacs.d/site-lisp/emacs-application-framework and
;; run 'git pull' and 'install-eaf.py' (M-x eaf-install-and-update) to update EAF
;;
;;(use-package eaf
;;  :load-path "~/.emacs.d/site-lisp/emacs-application-framework" ; Set to "/usr/share/emacs/site-lisp/eaf" if installed from AUR
;;  :custom
;;  ; See https://github.com/emacs-eaf/emacs-application-framework/wiki/Customization
;;  (eaf-browser-continue-where-left-off t)
;;  (eaf-browser-enable-adblocker t)
;;  (browse-url-browser-function 'eaf-open-browser)
;;  :config
;;  (defalias 'browse-web #'eaf-open-browser)
;;;;  (eaf-bind-key scroll_up "C-n" eaf-pdf-viewer-keybinding)
;;;;  (eaf-bind-key scroll_down "C-p" eaf-pdf-viewer-keybinding)
;;;;  (eaf-bind-key take_photo "p" eaf-camera-keybinding)
;;;;  (eaf-bind-key nil "M-q" eaf-browser-keybinding)
;;  ) ;; unbind, see more in the Wiki
;;
;;;;(require 'eaf-browser)
;;;;(require 'eaf-pdf-viewer)
;;(add-to-list 'load-path "~/.emacs.d/site-lisp/emacs-application-framework/")
;;(require 'eaf)
;;(require 'eaf-demo)
;;(require 'eaf-browser)
